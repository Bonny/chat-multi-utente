#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <signal.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>	
#include <sys/types.h>	
#include <arpa/inet.h>
#include "_regex_.h"
#include "chat-client.h"
void closeChat();
/* variabili globali */
user_t user; /* conterrà le credenzili dell'utente */
int run = 1;
int sd;

int main(int argc, char *argv[]) {

    int tentativi, connesso;
    msg_t request;
    pthread_t thread_stdin; /* thread per leggere i messaggi da STDIN e inviarli al server */
    pthread_t thread_stdout; /* thread per leggere i messaggi dal server e scriverli su STDOUT */
    struct sockaddr_in sa;

    request.type = MSG_LOGIN; /* valore di default */

    /* controllo il numero di argomenti passati */
    if (argc > 7 && argc < 5) {
        fprintf(stderr, "ERRORE: Impossiblie avviare la chat-client argomenti non corretti\n");
        exit(-1);
    }

    /* controllo validità degli argomenti e inizializzazione della struttura ut */
    if (argc == 5) { /* $chat-client Name Surname email username */
        strcpy(user.name, argv[1]);
        strcpy(user.surname, argv[2]);
        strcpy(user.email, argv[3]);
        strcpy(user.userName, argv[4]);
    } else if (argc == 6) { /* $chat-client -h | -r Name Surname email username */
        if (strlen(argv[1]) == 2) {
            if (strcmp(argv[1], "-h") == 0) {
                mostraComandi();
            } else if (strcmp(argv[1], "-r") == 0) {
                request.type = MSG_REGLOG;
            } else {
                fprintf(stderr, "ERRORE: Impossiblie avviare la chat-client argomenti non corretti\n");
                exit(-1);
            }
            strcpy(user.name, argv[2]);
            strcpy(user.surname, argv[3]);
            strcpy(user.email, argv[4]);
            strcpy(user.userName, argv[5]);
        }
    } else if (argc == 7) { /* $chat-client -h -r Name Surname email username */
        if ((strlen(argv[1]) == 2) && (strcmp(argv[1], "-h") == 0) && (strlen(argv[2]) == 2) && (strcmp(argv[2], "-r") == 0)) {
            mostraComandi();
            strcpy(user.name, argv[3]);
            strcpy(user.surname, argv[4]);
            strcpy(user.email, argv[5]);
            strcpy(user.userName, argv[6]);
            request.type = MSG_REGLOG;
        } else {
            fprintf(stderr, "ERRORE: Impossiblie avviare la chat-client argomenti non corretti\n");
            exit(-1);
        }
    }

    int g;
    for (g = 1; g < argc; g++)fprintf(stderr, "%s\n", argv[g]);

    /* signal per gestire il segnale SIGINT */
    if (signal(SIGINT, sigHand) == SIG_ERR) {
        fprintf(stderr, "ERRORE: Signal SIGINT fallita\n");
        exit(-1);
    }
    /* signal per gestire il segnale SIGTERM */
    if (signal(SIGTERM, sigHand) == SIG_ERR) {
        fprintf(stderr, "ERRORE: Signal SIGTERM fallita\n");
        exit(-1);
    }
    /* creazione del socket */
    if ((sd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        fprintf(stderr, "ERRORE: Creazione socket fallita\n");
        exit(-1);
    }
    /* inizializzazione dell'indirizzo del server */
    sa.sin_family = AF_INET;
    sa.sin_port = htons(PORTA);
    sa.sin_addr.s_addr = inet_addr("127.0.0.1");

    /* richiesta di connessione al server max 10 tentativi */

    tentativi = connesso = 0;

    while (tentativi < 10 && !connesso) {
        if ((connect(sd, (struct sockaddr*) &sa, sizeof (sa))) < 0) {
            tentativi++;
            sleep(1);
        } else connesso = 1;
        fprintf(stdout, "Tentativo = %d\n", tentativi);
    }

    if (tentativi == 10) {
        fprintf(stderr, "ERRORE: Connessione fallita - numero tentativi = %d\n", tentativi);
        exit(-1);
    }
    fprintf(stdout, "Connessione ok %d\n", tentativi);
    fprintf(stdout, "Attendi Login in corso....\n");



    /* inizializzo il messaggio request per la richiesta 
     * di connessione/registrazione al servizio */
    if (request.type == MSG_REGLOG) {
        char tmp[(256 * 3) + 3];
        strcpy(tmp, user.userName);
        strcpy(tmp + strlen(tmp), ":");
        strcpy(tmp + strlen(tmp), user.name);
        strcpy(tmp + strlen(tmp), ":");
        strcpy(tmp + strlen(tmp), user.surname);
        strcpy(tmp + strlen(tmp), ":");
        strcpy(tmp + strlen(tmp), user.email);
        request.msg = (char*) malloc(sizeof (char) * (strlen(tmp) + 1));
        strcpy(request.msg, tmp);
        request.msglen = strlen(tmp);
        request.msg[request.msglen] = '\0';
    } else {
        request.msg = (char*) malloc(sizeof (char) * (strlen(user.userName)));
        strcpy(request.msg, user.userName);
        request.msglen = strlen(user.userName);
        request.msg[request.msglen] = '\0';
    }

    //fprintf(stderr, "M->[%c][%s][%s][%d][%s]\n", request.type, request.sender, request.receiver, request.msglen, request.msg);

    /* inizializzo il buffer per inviare  la richiesta 
     * di connessione/registarzione al server  */

    char *bufReq;
    int sizeBufReq;

    preparedBuffer(&bufReq, request, &sizeBufReq);
    /*
        fprintf(stderr, "\nbuffer->[");

        int i;
        for (i = 0; i < sizeBufReq; i++)fprintf(stderr, "%c", bufReq[i]);
        fprintf(stderr, "]\n");
     */
    write(sd, bufReq, sizeBufReq);

    resetMsg(&request);

    readMsg(&sd, &request);

    if (request.type == MSG_OK) {
        fprintf(stdout, "SERVER: Login effettuato con successo\n");
    } else {
        fprintf(stderr, "\nERRORE: Accesso fallito\n");
        exit(-1);
    }

    if (pthread_create(&thread_stdin, NULL, write_msg_workers, (void*) sd)) {
        fprintf(stderr, "ERRORE: Creazione thread fallita\n");
        exit(-1);
    }
    if (pthread_create(&thread_stdout, NULL, read_msg_workers, (void *) sd)) {
        fprintf(stderr, "ERRORE: Creazione thread fallita\n");
        exit(-1);
    }

    if (pthread_join(thread_stdin, NULL)) {
        fprintf(stderr, "ERRORE: pthread_join\n");
        exit(-1);
    }

    if (pthread_join(thread_stdout, NULL)) {
        fprintf(stderr, "ERRORE: pthread_join\n");
        exit(-1);
    }
    return 0;
}

/* ********************************************************
 * funzione associata al thread in ascolto su STDIN       *
 * ******************************************************** */
void *write_msg_workers(void *t) {

    msg_t M;
    char *buffer = NULL;
    int sock, sizeBuffer;

    sock = (int) t;

    while (run) {

        resetMsg(&M);

        /* se inizializzazione di M è andata a buon fine */
        if (writeMsg(&M) > 0) {
            //fprintf(stderr, "*****contenuto di g:\n");
            //fprintf(stderr, "SEND M->[%c][%s][%s][%d][%s]\n", M.type, M.sender, M.receiver, M.msglen, M.msg);
            /* preparo il buffer */

            preparedBuffer(&buffer, M, &sizeBuffer);
            /* scrivo il buffer sul socket */
            write(sock, buffer, sizeBuffer);

            /* reset delle variabili M e sizeBuffer e del buffer */

            sizeBuffer = 0;
            free(buffer);

        } else {
            fprintf(stderr, "ERRORE: Sintassi del comando errata\n");
        }
    }
    closeChat();
    pthread_exit(NULL);
}

/* *********************************************************
 * funzione associata al thread in ascolto su STDOUT       *
 * ******************************************************* */
void *read_msg_workers(void *t) {

    int sock;
    msg_t M;

    sock = (int) t;

    while (run) {

        resetMsg(&M);

        readMsg(&sock, &M);

        if (M.type == MSG_OK) {
            fprintf(stdout, "\nOK\n");
        } else if (M.type == MSG_SINGLE) {
            fprintf(stdout, "%s:%s:%s\n", M.sender, M.receiver, M.msg);
        } else if (M.type == MSG_BRDCAST) {
            fprintf(stdout, "%s:*:%s\n", M.sender, M.msg);
        } else if (M.type == MSG_LIST) {

            if (M.msglen > 0) {

                fprintf(stdout,"LISTA USER: [%s]\n",M.msg);
            } else fprintf(stdout, "SERVER: Nessun utente connesso al servizio\n");

        } else if (M.type == MSG_ERROR) {
            fprintf(stderr, "ERRORE: %s\n", M.msg);
        }
    }
    pthread_exit(NULL);
}

/* **********************************************************************
 * funzione che legge (N * 2) + 5 elementi dalla socket                 *
 * che corrispondono  |type(1)|sender(256)|receiver(256)|lenmsg(4)|     *
 * successivamante legge lenmsg elementi dalla socket che corrispondono *
 * al testo del messaggio, e inizializza un struttura di tipo msg_t     *
 * ********************************************************************** */
void readMsg(int *sock, msg_t *m) {


    const int sizeDefaul = (N * 2) + 5;
    int sizeTesto;
    char buffer[sizeDefaul], len[4];

    /* leggo i primi N*2 +5 elementi dalla socket */
    read(*sock, buffer, sizeDefaul);
    m->type = buffer[0];
    strncpy(m->sender, buffer + 1, N);
    m->sender[strlen(m->sender)] = '\0';
    strncpy(m->receiver, buffer + N + 1, N);
    m->receiver[strlen(m->receiver)] = '\0';
    /* leggo la dimensione del testo del messaggio */
    strncpy(len, buffer + (N * 2) + 1, 4);
    sizeTesto = atoi(len);
    m->msglen = sizeTesto;
    if (sizeTesto > 0) {
        m->msg = (char*) malloc(sizeof (char) * sizeTesto);
        /* leggo dalla socket il testo del messaggio */
        read(*sock, m->msg, sizeTesto);
        m->msg[m->msglen] = '\0';
    }
}

/* *********************************************************
 * funzione legge da stdin i comandi inseriti dall'utente  *
 * e inizializza la struttura dati di tipo msg_t           *
 * ********************************************************* */
int writeMsg(msg_t *t) {

    int ret, of, k;
    char str[1000], tmp[1000], dest[256];

    fflush(stdin);

    gets(str);

    /* se il comando rispetta la politica del servizio */
    if (match(str, RGX_GENERIC)) {
        /* se il comando è del tipo '#dest UserName:testo\n' */
        if (match(str, RGX_DEST)) {
            //printf("\nsrt = [%s]\n", str);
            /* copio la stringa 'UserName:testo\n' da str */
            strcpy(tmp, str + 6);
            //fprintf(stderr, "tmp = [%s]\n", tmp);
            /* calcolo l'offset del carattere ':' per prelevare la stringa 'UserName' */
            of = strcspn(tmp, ":");
            //fprintf(stderr, "of = [%d]\n", of);
            /* prelevo la stringa 'UserName' */
            for (k = 0; k < of; k++)dest[k] = tmp[k];
            dest[k] = '\0';
            //fprintf(stderr, "dest = [%s]\n", dest);
            strcpy((*t).receiver, dest);
            of++;
            t->msg = (char*) malloc(sizeof (char) * (strlen(tmp) - of));
            //fprintf(stderr, "msg = [%s]\n", tmp + of + 1);
            /* prelevo la stringa 'testo\n' (msg+1 per scartare ':') */
            strcpy(t->msg, tmp + of);
            t->msg[strlen(t->msg)] = '\0';
            t->msglen = strlen(t->msg);
            t->type = MSG_SINGLE;
            strcpy(t->sender, user.userName);
            /* se il comando è del tipo '#dest:testo\n' */
        } else if (match(str, RGX_DEST_LESS)) {
            /* calcolo l'offset del carattere ':' per prelevare la stringa 'testo\n' */
            of = strcspn(str, ":");
            of++;
            t->msg = (char*) malloc(sizeof (char) *(strlen(str) - of));
            strcpy(t->msg, str + of);
            t->msg[strlen(t->msg)] = '\0';
            t->msglen = strlen(t->msg);
            t->type = MSG_BRDCAST;
            //fprintf(stderr, "\n[%s]\n", (*t).msg);
            strcpy(t->sender, user.userName);
            /* se il comando è del tioo '#logout' */
        } else if (match(str, RGX_LOGOUT)) {
            t->type = MSG_LOGOUT;
            strcpy(t->sender, user.userName);
            t->msglen = 0;

            run = 0;
            /* se il comando è del tipo '#ls' */
        } else if (match(str, RGX_LS_USERS)) {
            t->type = MSG_LIST;
            strcpy(t->sender, user.userName);
            t->msglen = 0;
        } else if(match(str, RGX_LS_USERS)){printf("ciao\n");
            mostraComandi();
        }
        ret = 1;
    } else {
        ret = 0;
    }
    return ret;
}

/* ***************************************************************
 * funzione che inizializza dimensione e contenuto del buffer    *
 * mediante il contenuto dei campi della struttura di tipo msg_t *
 * *************************************************************** */
void preparedBuffer(char **buf, msg_t m, int *size) {

    char len[4];

    *size = ((N * 2) + 5 + m.msglen);
    *buf = (char *) malloc(sizeof (char) * (*size));
    *buf[0] = m.type;
    strcpy(*buf + 1, m.sender);
    strcpy(*buf + N + 1, m.receiver);
    itoa(m.msglen, len);
    strcpy(*buf + (N * 2) + 1, len);

    if (m.msglen > 0) {
        strcpy(*buf + (N * 2) + 5, m.msg);
        free(m.msg);
    }


}

/* ************************************************************
 * funzione (handler) che gestisce i segnali SIGTERM e SIGINT *
 * ************************************************************ */
void sigHand(int sig) {

    if (sig == SIGTERM) {
        closeChat();
    } else if (sig == SIGINT) {
        closeChat();
    }
}

/* **********************************************************************
 * funzione che invia un messaggio di logout se l'utente chiude la chat *
 * ********************************************************************** */
void closeChat() {
    msg_t M;
    int sizeBuffer;
    char*buffer;

    M.type = MSG_LOGOUT;
    strcpy(M.sender, user.userName);
    M.msglen = 0;
    preparedBuffer(&buffer, M, &sizeBuffer);
    write(sd, buffer, sizeBuffer);
    free(buffer);

    run = 0;

    close(sd);
    exit(0);

}

/* ******************************************************
 * funzione che azzera il contenuto dei campi di una    *
 * struttura dati del tipo msg_t                        *
 * ****************************************************** */
void resetMsg(msg_t *t) {
    int k;
    for (k = 0; k < N; k++) {
        t->receiver[k] = '\0';
        t->sender[k] = '\0';
    }
    t->msg = NULL;
    t->msglen = 0;
    t->type = '\0';
}
