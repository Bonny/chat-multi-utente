#ifndef REGEX_H
#define REGEX_H

#include <sys/types.h>
#include <regex.h>

/* pattern per input stdin dal client*/

/* invio messaggio ad un singolo destinatario */
#define RGX_DEST "^([#][d][e][s][t])([ ]{1})[a-zA-Z0-9]{1,256}[:]([ ]*)[a-zA-Z0-9]+"
/* invio messaggio in broadcast*/
#define RGX_DEST_LESS "^([#][d][e][s][t])[:]([ ]*)[a-zA-Z0-9]+"
/* logout client*/
#define RGX_LOGOUT "([#][l][o][g][o][u][t])$"
/* visualizza la lista degli utenti connessi al servizio */
#define RGX_LS_USERS "([#][l][s])$"

#define RGX_GENERIC "("RGX_DEST")|("RGX_DEST_LESS")|("RGX_LOGOUT")|("RGX_LS_USERS")"

// Ritorna:
//  -1: Errore nella creazione del pattern
//   0: Pattern non trovato
//   1: Pattern trovato

int match(const char *s, char *regex) {

    regex_t re;

    // Compilo un pattern regex_t associato alla mia regex
    if (regcomp(&re, regex, REG_EXTENDED | REG_NOSUB) != 0)
        return -1;

    // Applico il pattern alla stringa passata
    if (regexec(&re, s, (size_t) 0, NULL, 0)) {
        regfree(&re);
        return 0;
    }

    regfree(&re);
    return 1;
}

#endif