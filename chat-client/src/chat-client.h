#ifndef CLIENT_H
#define CLIENT_H
/* porta di ascolto del server*/
#define PORTA 1745  

/* tipi di messaggio */
#define MSG_LOGIN    'L'
#define MSG_REGLOG   'R'
#define MSG_OK       'O'
#define MSG_ERROR    'E'
#define MSG_SINGLE   'S'
#define MSG_BRDCAST  'B'
#define MSG_LIST     'I'
#define MSG_LOGOUT   'X'

#define N 256

/* **********************************************************
 * stuttura con la quale clien e server si scambiano messaggi
 * ********************************************************** */
typedef struct {
    char type;
    char sender[N];
    char receiver[N];
    int msglen;
    char *msg;
} msg_t;

/* *********************************************************
 * struttura che contiene le credenziali di accesso 
 * al servizio dell'utente
 * ********************************************************* */
typedef struct {
    char userName[N];
    char name[N];
    char surname[N];
    char email[N];
} user_t;

void mostraComandi();
void sigHand(int);
void * write_msg_workers(void *);
void * read_msg_workers(void *);
int writeMsg(msg_t *);
void readMsg(int *, msg_t *);
void preparedBuffer(char **, msg_t, int*);
void resetMsg(msg_t*);

void mostraComandi() {
    fprintf(stdout, "+--------------------------------------------------------+\n");
    fprintf(stdout, "| Lista comandi utente chatFe:                           |\n");
    fprintf(stdout, "|                                                        |\n");
    fprintf(stdout, "| #dest Destinatario:testo - invia testo al destinatario |\n");
    fprintf(stdout, "| #dest :testo - invia testo in modalità broadcast       |\n");
    fprintf(stdout, "| #ls - visualizza la lista degli utenti connessi        |\n");
    fprintf(stdout, "| #logout - effettua il logout                           |\n");
    fprintf(stdout, "+--------------------------------------------------------+\n");
}

/* ********************************************
 * funzione reverse di un array di aratteri 
 * ******************************************** */
void reverse(char *s) {
    int c, i, j;
    for (i = 0, j = strlen(s) - 1; i < j; i++, j--) {
        c = s[i];
        s[i] = s[j];
        s[j] = c;
    }
}

/* ************************************************
 * converte un intero n in un array di char *s 
 * ************************************************ */
char * itoa(int n, char *s) {

    int i, sign;

    if ((sign = n) < 0) /* tiene traccia del segno */
        n = -n; /* rende n positivo */
    i = 0;
    do { /* genera le cifre nell'ordine inverso */
        s[i++] = n % 10 + '0'; /* estrae la cifra seguente */
    } while ((n /= 10) > 0); /* elimina la cifra da n */
    if (sign < 0)
        s[i++] = '-';
    s[i] = '\0';
    reverse(s);

    return s;
}

#endif