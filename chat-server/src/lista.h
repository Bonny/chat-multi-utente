#ifndef LISTA_H
#define LISTA_H 

typedef struct cella * lista;
typedef lista          posizione;
typedef void *         tipoelemento;

struct cella {
  posizione   	precedente;
  tipoelemento  elemento;
  posizione   	successivo;
};


/* CREALISTA
    crea una cella che ha come precedecessore e successore se stessa
    questa e' la cella sentinella
*/
lista CREALISTA () {
  lista L;
  L = (lista) malloc(sizeof (struct cella) ); 
  L->successivo = L;
  L->precedente = L;
  return L;
}


/* LISTAVUOTA
   una lista e' vuota se contiene soltanto la lista sentinella
*/
int LISTAVUOTA (lista L) {
  int listavuota;
  listavuota = ((L->successivo == L) && (L->precedente == L)) ? 1 : 0;
  return listavuota;
}

  
posizione PRIMOLISTA (lista L) {
  return L->successivo;
}


posizione ULTIMOLISTA (lista L) {
  return L->precedente;
}


posizione SUCCLISTA (posizione p) {
  return p->successivo;
}


posizione PREDLISTA (posizione p) {
  return p->precedente;
}

/* FINELISTA
   una posizione e' fuori dalla sequenza se p punta alla lista sentinella
*/
int FINELISTA (posizione p, lista L) {
  int finelista;
  finelista = (p == L) ? 1 : 0;
  return finelista;
} 


void * LEGGILISTA (posizione p) {
  return p->elemento;
}


void SCRIVILISTA (void * a, posizione p) {
  p->elemento = a;
}


/* INSLISTA: supponiamo che p punti alla cella i-esima
    - la cella i diviene i+1
    - viene creata una nuova cella che prende il posto i
    - il valore di p viene aggiornato con l'indirizzo della nuova cella  
*/
void INSLISTA (void  *a, posizione * p) {
  struct cella * tmp;
  
  tmp = (struct cella *) malloc(sizeof(struct cella));
  
  tmp->precedente = (*p)->precedente; 
  tmp->successivo = (*p);
  tmp->elemento   = a;
  
  (*p)->precedente->successivo = tmp;
  (*p)->precedente             = tmp;
  
  (*p) = tmp;
}

/*  p passato per variabile in accordo alla specifica */
void CANCLISTA (posizione * p) {
  posizione tmp;
  
  tmp = (*p);
  
  (*p)->precedente->successivo = (*p)->successivo;
  (*p)->successivo->precedente = (*p)->precedente;
  
  (*p) = (*p)->successivo;
  
  free(tmp);  
}

#endif

