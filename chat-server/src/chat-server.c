#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <time.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>	
#include <arpa/inet.h>
#include "chat-server.h"
#include "lista.h"
#include "hash.h"
void closeServer();
/* variabili globali */
FILE *log_fp = NULL, *user_fp = NULL;
int run = 1;
hash H = NULL;

/* Mutex e variabile di condizione per mutua escusione 
 * fra i thread workes per registrare eventi nel file-log  */
pthread_mutex_t muxLog;
pthread_cond_t condLog;
int varCondLog = LIBERO;

int main(int argc, char *argv[]) {

    struct sockaddr_in client_addr; /* indirizzo del client */
    struct sockaddr_in server_addr; /* indirizzo del server */
    int sd_server, sd_client;
    int i;

    /* inizializzazione var globali */
    if ((H = CREAHASH()) == NULL) {
        fprintf(stderr, "ERRORE: Impossibile avviare il server creazione Hash fallita\n");
        exit(-1);
    }
    pthread_mutex_init(&muxLog, NULL);
    pthread_cond_init(&condLog, NULL);
    /* controllo numero di srgomenti passati */
    if (argc < 3) {
        fprintf(stderr, "ERRORE: Impossibile avviare il server\n");
        exit(-1);
    }

    for (i = 1; i < argc; i++)printf("[%d]\t%s\n", i, argv[i]);
    printf("\n");
    /* associazione handler per signal */
    if (signal(SIGINT, sigHand) == SIG_ERR) {
        fprintf(stderr, "ERRORE: Signal SIGINT fallita\n");
        exit(-1);
    } else fprintf(stdout, "SERVER: SIGINT ok\n");
    if (signal(SIGTERM, sigHand) == SIG_ERR) {
        fprintf(stderr, "ERRORE: Signal SIGTERM fallita\n");
        exit(-1);
    } else fprintf(stdout, "SERVER: SIGTERM ok\n");
    /* open file-user e log-file */
    if ((user_fp = fopen(argv[1], "r")) == NULL) {
        fprintf(stderr, "ERRORE: Impossibile aprire il file [%s]\n", argv[1]);
        exit(-1);
    } else fprintf(stdout, "SERVER: Open user-file ok\n");
    if ((log_fp = fopen(argv[2], "w")) == NULL) {
        fprintf(stderr, "ERRORE: Impossibile aprire il file [%s]\n", argv[2]);
        exit(-1);
    } else fprintf(stdout, "SERVER: Open log-file ok\n");
    fflush(stdout);
    /* leggo i record di file-user inizializzo un astruttura elem_t
     * e la inserisco nella tabella hash                             */
    fprintf(stdout, "\nLista User:\n");
    elem_t *e;
    int eof = 1;
    while (eof) {
        e = (elem_t*) malloc(sizeof (elem_t));
        if (readUsers(user_fp, &(e->user)) > 0) {
            fprintf(stdout, "[%s][%s][%s][%s]\n", e->user.userName, e->user.name, e->user.surname, e->user.email);
            insertToHash(e->user.name, e, H);
        } else eof = 0;
        e = NULL;
    }
    fprintf(stdout, "\n");
    fclose(user_fp);
    fflush(stdout);

    /* Creazione socket descriptor per il server.
     AF_INET + SOCK_STREAM --> TCP, utilizzo del protocollo TCP (IPPROTO_TCP)*/

    if ((sd_server = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        fprintf(stderr, "SERVER: Errore nella creazione del socket\n");
        exit(-1);
    } else fprintf(stdout, "SERVER: Socket ok\n");
    int optval = 1;
    if (setsockopt(sd_server, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof (optval)) < 0) {
        fprintf(stderr, "SERVER: Errore riuso porta\n");
        exit(-1);
    } else fprintf(stdout, "SERVER: Opzioni porta ok\n");

    bzero(&server_addr, sizeof (struct sockaddr_in));
    /* Inseriamo nella struttura alcune informazioni */
    server_addr.sin_family = AF_INET; /* la famiglia dei protocolli */
    server_addr.sin_port = htons(PORTA); /* la porta in ascolto */
    server_addr.sin_addr.s_addr = INADDR_ANY; /* dato che è un server bisogna associargli l'indirizzo della macchina su cui sta girando */
    /* Assegnazione del processo alla socket tramite la funzione BIND */
    if (bind(sd_server, (struct sockaddr *) &server_addr, sizeof (server_addr)) < 0) {
        fprintf(stderr, "SERVER: Errore di binding\n");
        exit(-1);
    } else fprintf(stdout, "SERVER: Bind ok\n");
    /* Si mette in ascolto con un massimo di 20 connessioni */
    listen(sd_server, 20);
    fprintf(stdout, "SERVER: In Ascolto ...\n");

    int address_size = sizeof (client_addr); /* dimensione della struttura client_addr */

    while (run) {

        sd_client = accept(sd_server, (struct sockaddr *) &client_addr, &address_size);
        if (sd_client < 0) {
            fprintf(stdout, "SERVER: Errore nella chiamata accept\n");
            exit(-1);
        } else {
            fprintf(stdout, "SERVER: Accept client [Ip: %d]\n", client_addr.sin_port);
            pthread_t T;
            pthread_attr_t attr;
            /* Initialize and set thread detached attribute */
            pthread_attr_init(&attr);
            pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

            if (pthread_create(&T, &attr, workers, (void *) sd_client)) {
                fprintf(stderr, "ERRORE: creazione thread fallita\n");
            } else {
                pthread_attr_destroy(&attr);
            }

            fprintf(stdout, "SERVER: In Ascolto ...\n");
        }

        fflush(stdout);
    }

    return 0;
}

/* ******************************************************
 * 
 *                         
 * ****************************************************** */
void *workers(void *t) {

    lista L;
    posizione p;
    msg_t M;
    user_t ut;
    elem_t *el;
    int sock, go, sizeb;
    char *buf;

    sock = (int) t;
    go = 1;

    while (run && go) {

        L = p = NULL;
        el = NULL;
        /* leggo il messaggio dalla socket */

        resetMsg(&M);

        readMsg(&sock, &M);

        fprintf(stderr, "sock = [%d] Messaggio letto: M->[%c][%s][%s][%d][%s]\n", sock, M.type, M.sender, M.receiver, M.msglen, M.msg);

        /* se è una richiesta di login */
        if (M.type == MSG_LOGIN) {

            fprintf(stderr, "sock = [%d] Richiesta LOGIN \n", sock);
            /* smonto la stringa msg e prelevo le credenziali del client */
            resetUser(&ut);
            strcpy(ut.userName, M.msg);

            /* cerco in 'H' l'utente mediante userName,'el' puntatore
             * alla struttura associatata all'utente  all'interno di 'H' */
            searchElemToHash(ut.userName, &el, H);

            /* se utente 'el' esiste nella tabella hash */
            if (el != NULL) {

                writeToFileLog(0, ut.userName, NULL, NULL);

                /* inizializzo gli attributi della struttura puntata da 'el' */
                pthread_mutex_init(&(el->mux), NULL);
                pthread_cond_init(&(el->cond), NULL);
                el->isConn = 1;
                el->sd = sock;
                el->var_cond = LIBERO;
                resetMsg(&M);
                M.type = MSG_OK;
                M.msglen = 0;
                writeToSocket(el, M);

                fprintf(stderr, "sock = [%d] LOGIN OK\nsock = [%d] struttura utente: el->[%s][%s][%s][%s]\n", sock, sock, el->user.userName, el->user.name, el->user.surname, el->user.email);
            } else {

                fprintf(stderr, "sock = [%d] LOGIN NEGATO\n", sock);

                go = 0;
                resetMsg(&M);
                M.type = MSG_ERROR;
                M.msglen = 0;
                preparedBuffer(&buf, M, &sizeb);
                write(sock, buf, sizeb);
                free(buf);
                sizeb = 0;
            }
            /* risposta al messaggio di login */

            /* se è una richiesta di registrazione e login */
        } else if (M.type == MSG_REGLOG) {

            fprintf(stderr, "sock = [%d] Richiesta REG-LOG \n", sock);

            parseString(M.msg, &ut);

            searchElemToHash(ut.userName, &el, H);

            /* se utente 'el' esiste nella tabella hash */
            if (el == NULL) {

                el = (elem_t*) malloc(sizeof (elem_t));

                strcpy(el->user.userName, ut.userName);
                strcpy(el->user.name, ut.name);
                strcpy(el->user.surname, ut.surname);
                strcpy(el->user.email, ut.email);
                pthread_mutex_init(&(el->mux), NULL);
                pthread_cond_init(&(el->cond), NULL);
                el->isConn = 1;
                el->sd = sock;
                el->var_cond = LIBERO;

                /* e lo inserisco in hash */
                insertToHash(ut.name, el, H);
                resetMsg(&M);

                M.type = MSG_OK;
                M.msglen = 0;

                writeToFileLog(0, ut.userName, NULL, NULL);
                fprintf(stderr, "sock = [%d] REG-LOG OK\nsock = [%d] Struttura utente: el->[%s][%s][%s][%s]\n", sock, sock, el->user.userName, el->user.name, el->user.surname, el->user.email);

                /* risposta */
                writeToSocket(el, M);

            } else {

                fprintf(stderr, "sock = [%d] REG-LOGIN NEGATO\n", sock);

                go = 0;
                resetMsg(&M);
                M.type = MSG_ERROR;
                M.msglen = 0;
                preparedBuffer(&buf, M, &sizeb);
                write(sock, buf, sizeb);
                free(buf);
                sizeb = 0;
            }
            /* rispondo alla richiesta */

            /* se è un messaggio da inviare ad un singolo utente */
        } else if (M.type == MSG_SINGLE) {


            searchElemToHash(M.receiver, &el, H);

            if (el != NULL) {
                if (el->isConn && (strcmp(M.sender, M.receiver) != 0)) {

                    writeToFileLog(2, M.sender, M.receiver, M.msg);
                    writeToSocket(el, M);
                    fprintf(stderr, "sock = [%d] SEND MSG SINGLE (%s)--->(%s)\n", sock, M.sender, M.receiver);
                    if (M.msglen > 0)free(M.msg);
                }
            }

            /* se è un messaggio da inviare in broadcast */
        } else if (M.type == MSG_BRDCAST) {

            getListUserHash(&L, H);

            p = PRIMOLISTA(L);

            while (!FINELISTA(p, L)) {

                el = (elem_t *) (p->elemento);

                if ((strcmp(el->user.userName, M.sender) != 0) && (el->isConn == 1)) {

                    writeToSocket(el, M);

                    writeToFileLog(2, M.sender, el->user.userName, M.msg);

                    fprintf(stderr, "sock = [%d] SEND MSG BRD (%s)--->(%s)\n", sock, M.sender, el->user.userName);

                }
                p = SUCCLISTA(p);
            }

            /* se è una richiesta #ls */
        } else if (M.type == MSG_LIST) {

            char *tmp, *str;
            int lenght = 0, count = 0;
            lista list;
            posizione pos;

            list = CREALISTA();

            getListUserHash(&L, H);

            p = PRIMOLISTA(L);

            while (!FINELISTA(p, L)) {

                el = (elem_t *) (p->elemento);

                if ((strcmp(el->user.userName, M.sender) != 0) && (el->isConn == 1)) {

                    pos = PRIMOLISTA(list);
                    lenght += strlen(el->user.userName);
                    count++;
                    INSLISTA((void*) el->user.userName, &pos);
                }
                p = SUCCLISTA(p);
            }

            if (count > 0) {
                tmp = (char*) malloc(sizeof (char) *(lenght + 1));
                pos = PRIMOLISTA(list);
                while (!FINELISTA(pos, list)) {
                    str = (char*) pos->elemento;
                    strcpy(tmp + strlen(tmp), str);
                    strcpy(tmp + strlen(tmp), ":");
                    pos = SUCCLISTA(pos);
                }

                el = NULL;
                searchElemToHash(M.sender, &el, H);
                M.msg = tmp;
                M.msg[strlen(M.msg)-1] = '\0';
                M.msglen = lenght + 1;
                fprintf(stderr,"------------->[%s]\n",M.msg);
                writeToSocket(el, M);
                free(tmp);
            }

        } else if (M.type == MSG_LOGOUT) {

            searchElemToHash(M.sender, &el, H);

            if (el != NULL) {

                fprintf(stderr, "sock = [%d] LOGOUT OK \n", sock);
                go = 0;
                el->var_cond = 0;
                writeToFileLog(1, M.sender, NULL, NULL);
            }
        }
    }

    printf("sock = [%d] CLIENT DISCONNESSO OK \n", sock);
    close(sock);
    pthread_exit(NULL);
}

/* **************************************************************
 * funzione che prepara il buffer contenente un messaggio       *
 * da scrivere sulla socket associata all'utente 'e'            *
 * questa operazione viene effettuata in muta esclusione        *
 * mediante l'ausilio del mutex e della variabile di condizione *
 * associata all'utente 'e', ovvero all'elemento della lista di *
 * trabocco della tabella hash.                                 *
 * ************************************************************** */
void writeToSocket(elem_t *e, msg_t m) {

    char *buffer;
    int sizeBuffer;
    /* fase di ingresso */
    pthread_mutex_lock(&(e->mux));
    while (varCondLog == OCCUPATO)pthread_cond_wait(&(e->cond), &(e->mux));
    varCondLog = OCCUPATO;
    pthread_mutex_unlock(&(e->mux));
    /* utilizzo la risorsa */
    preparedBuffer(&buffer, m, &sizeBuffer);
    write(e->sd, buffer, sizeBuffer);
    free(buffer);
    /* fase di uscita */
    pthread_mutex_lock(&(e->mux));
    varCondLog = LIBERO;
    pthread_cond_signal(&(e->cond));
    pthread_mutex_unlock(&(e->mux));
}

/* *********************************************************
 * funzione che prepara il record e lo scrive su file-log  *
 * questa operazione viene effettuata in muta esclusione   *
 * mediante l'utilizzo di muxLog  condLog.                 *
 * ********************************************************* */
void writeToFileLog(int type, char *arg1, char *arg2, char *arg3) {

    char time[64];
    /* fase di ingresso */
    pthread_mutex_lock(&muxLog);
    while (varCondLog == OCCUPATO)pthread_cond_wait(&condLog, &muxLog);
    varCondLog = OCCUPATO;
    pthread_mutex_unlock(&muxLog);

    /* utilizzo la risorsa */
    timeStamp(time);

    switch (type) {
        case 0: fprintf(log_fp, "%s:Login:%s\n", time, arg1);
            break;
        case 1: fprintf(log_fp, "%s:Logout:%s\n", time, arg1);
            break;
        case 2: fprintf(log_fp, "%s:%s:%s:%s\n", time, arg1, arg2, arg3);
            break;
        default: fprintf(log_fp, "%s\tERROR\n", time);
            break;
    }

    fflush(log_fp);
    /* fase di uscita */
    pthread_mutex_lock(&muxLog);
    varCondLog = LIBERO;
    pthread_cond_signal(&condLog);
    pthread_mutex_unlock(&muxLog);
}

/* ***************************************************************
 * funzione che inizializza dimensione e contenuto del buffer    *
 * mediante il contenuto dei campi della struttura di tipo msg_t *
 * *************************************************************** */
void preparedBuffer(char **buf, msg_t m, int *size) {

    char len[4];
    *size = ((N * 2) + 5 + m.msglen);
    *buf = (char *) malloc(sizeof (char) * (*size));
    *buf[0] = m.type;
    strcpy(*buf + 1, m.sender);
    strcpy(*buf + N + 1, m.receiver);
    itoa(m.msglen, len);
    strcpy(*buf + (N * 2) + 1, len);

    if (m.msglen > 0) {
        strcpy(*buf + (N * 2) + 5, m.msg);
        //free(m.msg);
    }


}

/* **********************************************************************
 * funzione che legge (N * 2) + 5 elementi dalla socket                 *
 * che corrispondono  |type(1)|sender(256)|receiver(256)|lenmsg(4)|     *
 * successivamante legge lenmsg elementi dalla socket che corrispondono *
 * al testo del messaggio, e inizializza un struttura di tipo msg_t     *
 * ********************************************************************** */
void readMsg(int *sock, msg_t *m) {

    const int sizeDefaul = (N * 2) + 5;
    int sizeTesto;
    char buffer[sizeDefaul], len[4];

    /* leggo i primi N*2 +5 elementi dalla socket */
    read(*sock, buffer, sizeDefaul);
    m->type = buffer[0];
    strncpy(m->sender, buffer + 1, N);
    m->sender[strlen(m->sender)] = '\0';
    strncpy(m->receiver, buffer + N + 1, N);
    m->receiver[strlen(m->receiver)] = '\0';
    /* leggo la dimensione del testo del messaggio */
    strncpy(len, buffer + (N * 2) + 1, 4);
    sizeTesto = atoi(len);
    m->msglen = sizeTesto;
    if (sizeTesto > 0) {
        m->msg = (char*) malloc(sizeof (char) * sizeTesto);
        /* leggo dalla socket il testo del messaggio */
        read(*sock, m->msg, sizeTesto);
        m->msg[m->msglen] = '\0';
    }
}

/* ******************************************************
 * funzione che legge i record corrispondenti a:        *
 * 'userName:name surname:email' effettuta il parser    *
 * della stringa e inizializza una strutture user_t     *
 * ****************************************************** */
int readUsers(FILE *fp, user_t *ut) {
    int ret, k, i;
    char a[400], b[400];

    resetUser(ut);

    if (fscanf(fp, "%s%s", a, b) > 0) {
        i = 0;
        while (a[i] != ':') {
            ut->userName[i] = a[i];
            i++;
        }
        ut->userName[i] = '\0';
        i++;
        k = 0;
        while (i < strlen(a)) {
            ut->name[k++] = a[i++];
        }
        ut->name[k++] = '\0';
        i = 0;
        while (b[i] != ':') {
            (*ut).surname[i] = b[i];
            i++;
        }
        ut->surname[i] = '\0';
        i++;
        k = 0;
        while (i < strlen(b)) {
            ut->email[k++] = b[i++];
        }

        ut->email[k] = '\0';

        ret = 1;
    } else ret = -1;

    return ret;
}

/* ******************************************************
 * funzione che azzera il contenuto dei campi di una    *
 * struttura dati del tipo msg_t                        *
 * ****************************************************** */


void resetMsg(msg_t *t) {
    int k;
    for (k = 0; k < N; k++) {
        t->receiver[k] = '\0';
        t->sender[k] = '\0';
    }
    t->msg = NULL;
    t->msglen = 0;
    t->type = '\0';
}

/* ******************************************************
 * funzione che azzera il contenuto dei campi di una    *
 * struttura dati del tipo user_t                        *
 * ****************************************************** */
void resetUser(user_t *t) {
    int k;
    for (k = 0; k < N; k++) {
        t->email[k] = '\0';
        t->name[k] = '\0';
        t->surname[k] = '\0';
        t->userName[k] = '\0';
    }
}

/* ************************************************************
 * funzione (handler) che gestisce i segnali SIGTERM e SIGINT *
 * ************************************************************ */
void sigHand(int sig) {
    if (sig == SIGTERM) {
        closeServer();
    } else if (sig == SIGINT) {
        closeServer();
    }
}

void closeServer() {

    lista L;
    posizione p;
    elem_t *e;

    run = 0;

    if ((user_fp = fopen("user-file", "w")) != NULL) {

        fprintf(stderr, "SERVER: Save Users\n");
        getListUserHash(&L, H);

        p = PRIMOLISTA(L);
        while (!FINELISTA(p, L)) {
            e = (elem_t*) p->elemento;
            fprintf(user_fp, "%s:%s %s:%s\n", e->user.userName, e->user.name, e->user.surname, e->user.email);
            p = SUCCLISTA(p);
        }

        fflush(user_fp);
        fclose(user_fp);
    }

    exit(0);
}

/* ******************************************************
 * 64
 *                         
 * ****************************************************** */
void timeStamp(char *ts) {

    time_t t;
    t = time(NULL);
    ctime_r(&t, ts);
    ts[strlen(ts) - 1] = '\0';
}

void parseString(char *str, user_t *ut) {

    int k = 0, j = 0, h = 0;
    String v[4];

    while (k < strlen(str)) {
        if (str[k] == ':') {
            v[j][h] = '\0';
            j++;
            h = 0;
        } else {
            v[j][h++] = str[k];
        }
        k++;
    }

    strcpy(ut->userName, v[0]);
    strcpy(ut->name, v[1]);
    strcpy(ut->surname, v[2]);
    strcpy(ut->email, v[3]);
}