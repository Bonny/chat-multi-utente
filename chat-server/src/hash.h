#ifndef HASH_H
#define HASH_H

#include "lista.h"
#include "chat-server.h"

/* numero primo */
#define HL 313 

typedef lista * hash;

//////////////////////////////////////////////////////////////////////

int hashfunc(char * k) {
    int i = 0;
    int hashval = 0;
    for (i = 0; i < strlen(k); i++) {
        hashval = ((hashval * 256) + k[i]) % HL;
    }
    return hashval;
}


//////////////////////////////////////////////////////////////////////

hash CREAHASH() {
    hash H;
    int i;
    H = (hash) malloc(HL * sizeof (lista));
    for (i = 0; i < HL; i++) {
        H[i] = CREALISTA();
    }
    return H;
}

void insertToHash(char * key, elem_t * data, hash H) {

    int i;
    posizione p;

    i = hashfunc(key);
    p = PRIMOLISTA(H[i]);
    INSLISTA((void*) data, &p);

}

void * isToHash(user_t ut, hash H) {

    int i, found;
    posizione p;
    lista L;
    elem_t *e;

    found = 0;
    i = hashfunc(ut.name);
    L = H[i];
    p = PRIMOLISTA(L);

    while (!FINELISTA(p, L) && !found) {
        e = (elem_t *) p->elemento;

        //fprintf(stderr, "elem_t->[%s][%s][%s][%s]\n", e->user.userName, e->user.name, e->user.surname, e->user.email);
        //fprintf(stderr, "ut    ->[%s][%s][%s][%s]\n", ut.userName, ut.name, ut.surname, ut.email);

        if (strcmp((*e).user.name, ut.name) == 0 &&
                strcmp((*e).user.surname, ut.surname) == 0 &&
                strcmp((*e).user.email, ut.email) == 0 &&
                strcmp((*e).user.userName, ut.userName) == 0) {

            found = 1;
        } else {
            e = NULL;
            p = SUCCLISTA(p);
        }
    }

    return (void *) e;

}

void searchElemToHash(char *usn, elem_t **e, hash H) {
    int i, found;
    elem_t *tmp;
    posizione p;
    lista L;

    i = found = 0;

    while (i < HL && !found) {

        L = H[i];
        p = PRIMOLISTA(L);

        while (!FINELISTA(p, L) && !found) {

            tmp = (elem_t*) p->elemento;

            if (strcmp(tmp->user.userName, usn) == 0) {
                //fprintf(stderr, "\n[%s]==[%s]\n", tmp->user.userName, usn);
                *e = tmp;
                found = 1;
            } else {
                p = SUCCLISTA(p);
            }
        }

        i++;
    }
}

void getListUserHash(lista *usersList, hash H) {

    lista L, listaTmp;
    posizione p, posTmp;
    elem_t *e;
    int i;

    listaTmp = CREALISTA();

    for (i = 0; i < HL; i++) {

        L = H[i];
        p = PRIMOLISTA(L);

        while (!FINELISTA(p, L)) {

            e = (elem_t*) p->elemento;

            //if ((strcmp(e->user.userName, sender) != 0) && (e->isConn)) {

                posTmp = PRIMOLISTA(listaTmp);
                INSLISTA(p->elemento, &posTmp);
            //}

            p = SUCCLISTA(p);
        }
    }

    *usersList = listaTmp;
}
#endif

