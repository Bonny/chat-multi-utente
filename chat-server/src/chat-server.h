#ifndef SERVER_H
#define SERVER_H
/* porta di ascolto del server*/
#define PORTA 1745  

#define LIBERO 1
#define OCCUPATO 0

/* tipi di messaggio */
#define MSG_LOGIN    'L'
#define MSG_REGLOG   'R'
#define MSG_OK       'O'
#define MSG_ERROR    'E'
#define MSG_SINGLE   'S'
#define MSG_BRDCAST  'B'
#define MSG_LIST     'I'
#define MSG_LOGOUT   'X'

#define N 256

/* definisco un tipo di dato stringa */
typedef char v[N];
typedef v String;

/* **********************************************************
 * stuttura con la quale clien e server si scambiano messaggi
 * ********************************************************** */
typedef struct {
    char type;
    char sender[N];
    char receiver[N];
    int msglen;
    char *msg;
} msg_t;

/* *********************************************************
 * struttura che contiene le credenziali di accesso        *
 * al servizio dell'utente                                 *
 * ********************************************************* */
typedef struct {
    char userName[N];
    char name[N];
    char surname[N];
    char email[N];
} user_t;

/* *********************************************************
 * struttura che rappresenta il tipo di elemento delle     *
 * liste di trabocco                                       *
 * ********************************************************* */
typedef struct {
    user_t user;
    pthread_mutex_t mux;
    pthread_cond_t cond;
    int sd;
    int isConn;
    int var_cond;
} elem_t;


void sigHand(int);

void resetMsg(msg_t *);
void resetUser(user_t *);

int readUsers(FILE *, user_t *);
void readMsg(int *, msg_t *);

void preparedBuffer(char **, msg_t , int *);

void *workers(void *);

void timeStamp(char *);

void parseString(char *, user_t *);


void writeToSocket(elem_t *, msg_t);
void writeToFileLog(int, char*, char*, char*);
/* ********************************************
 * funzione reverse di un array di aratteri   *
 * ******************************************** */
void reverse(char *s) {
    int c, i, j;
    for (i = 0, j = strlen(s) - 1; i < j; i++, j--) {
        c = s[i];
        s[i] = s[j];
        s[j] = c;
    }
}

/* ************************************************
 * converte un intero n in un array di char *s    *
 * ************************************************ */
char * itoa(int n, char *s) {

    int i, sign;

    if ((sign = n) < 0) /* tiene traccia del segno */
        n = -n; /* rende n positivo */
    i = 0;
    do { /* genera le cifre nell'ordine inverso */
        s[i++] = n % 10 + '0'; /* estrae la cifra seguente */
    } while ((n /= 10) > 0); /* elimina la cifra da n */
    if (sign < 0)
        s[i++] = '-';
    s[i] = '\0';
    reverse(s);

    return s;
}

#endif

